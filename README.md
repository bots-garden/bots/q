# Q

> Q is my new side project. Q will be a Kotlin 🤖 (bot), but for the beginning, it's a sandbox project with:

- RedPipe http://redpipe.net/
- Kotlin
- Arrow https://arrow-kt.io/

# Run it

- Type this: `mvn install exec:java -e` 
- or if you want to change http port: `PORT=8080 mvn install exec:java -e`

