package garden.bots

import arrow.core.None
import arrow.core.Option
import arrow.core.Some
import garden.bots.resources.HelloResource
import garden.bots.resources.StaticResource
import garden.bots.resources.YoResource
import io.vertx.core.json.JsonObject
import net.redpipe.engine.core.Server

fun main(args: Array<String>) {
  val config = JsonObject()

  val envHttpPort = Option.fromNullable(System.getenv("PORT"))

  val httpPort: Int = when(envHttpPort) {
    is None -> 8080
    is Some -> Integer.parseInt(envHttpPort.t)
  }

  config.put("http_port", httpPort)

  Server()
    .start(config,
      StaticResource::class.java,
      HelloResource::class.java,
      YoResource::class.java
    )
    .subscribe(
      { v -> println("RedPipe server is started") },
      { it.printStackTrace() }
    )
}
