package garden.bots.resources

import arrow.core.Failure
import arrow.core.Success
import arrow.core.Try
import net.redpipe.engine.resteasy.FileResource
import javax.ws.rs.GET
import javax.ws.rs.Path
import javax.ws.rs.PathParam
import javax.ws.rs.Produces
import javax.ws.rs.core.Response

@Path("/{path:(.*)?}")
class StaticResource : FileResource() {
  @Produces("text/html; charset=utf-8") @GET
  fun index(@PathParam("path") path: String): Response {

    val staticAsset: Try<Response> = Try {
      super.getFile(if (path == "") "index.html" else path)
    }

    return when(staticAsset) {
      is Failure -> super.getFile("error.html")
      is Success -> staticAsset.value
    }
  }
}
